import { Component, OnInit } from '@angular/core';
import { Cat } from '../models/cat';
import { BACKENDAPIService } from '../services/backend-api.service';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.sass']
})
export class CatsComponent implements OnInit {

  constructor(private http: BACKENDAPIService) { }

  cats:Cat[]|undefined;

  ngOnInit(): void {
    this.http.getCats().toPromise().then(data => this.cats = data);
    }

}
