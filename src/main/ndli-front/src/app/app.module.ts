import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatsComponent } from './cats/cats.component';
import { CardComponentComponent } from './card-component/card-component.component';
import { TicketComponentComponent } from './ticket-component/ticket-component.component';
import { DemandeComponentComponent } from './demande-component/demande-component.component';

@NgModule({
  declarations: [
    AppComponent,
    CatsComponent,
    CardComponentComponent,
    DemandeComponentComponent
    CardComponentComponent,
    TicketComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
