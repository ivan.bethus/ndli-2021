import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BACKENDAPIService, helloMsg } from './services/backend-api.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [BACKENDAPIService]
})

export class AppComponent implements OnInit{
  constructor(private apiService: BACKENDAPIService){}
 

  title = 'ndli-front';
  message: string | undefined; 


  ngOnInit(): void {
    console.log(this.loadHelloMsg());
    this.loadHelloMsg();
  }

  loadHelloMsg() {
    this.apiService.getHello()
      .toPromise().then((data) => {this.message = data});
  };
  
}