import { TestBed } from '@angular/core/testing';

import { BACKENDAPIService } from './backend-api.service';

describe('AICAPIService', () => {
  let service: BACKENDAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BACKENDAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
