import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, mergeMap, retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Cat } from '../models/cat';

export interface helloMsg {
  message: string
}

@Injectable()
export class BACKENDAPIService {
  baseURL = '/api';

  constructor(private http: HttpClient) { }

  getHello() {
    return this.http.get(this.baseURL.concat("/").concat("hello"), {responseType: 'text'});
  }

  getCats(){
    return this.http.get<Cat[]>(this.baseURL.concat("/").concat("cats/"));
  }
}
