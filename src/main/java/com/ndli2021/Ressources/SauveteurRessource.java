package com.ndli2021.Ressources;
import com.ndli2021.models.Sauveteur;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/sauveteurs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SauveteurRessource {

    @GET
    public List<Sauveteur> list(){
        return Sauveteur.listAll();
    }

    @GET
    @Path("/{id}")
    public Sauveteur get(@PathParam("id") Long id) {
        return Sauveteur.findById(id);
    }

    @POST
    @Transactional
    public Response create(Sauveteur sauveteur) {
        sauveteur.persist();
        return Response.created(URI.create("/sauveteurs/" + sauveteur.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public Sauveteur update(@PathParam("id") Long id, Sauveteur sauveteur) {
        Sauveteur entity = Sauveteur.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.bateaux = sauveteur.bateaux;
        entity.description = sauveteur.description;
        entity.famille = sauveteur.famille;
        entity.prenom = sauveteur.prenom;
        entity.sauvetages = sauveteur.sauvetages;
        entity.recompenses = sauveteur.recompenses;
        entity.temoignages = sauveteur.temoignages;
        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        Sauveteur entity = Sauveteur.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/count")
    public Long count() {
        return Sauveteur.count();
    }
}
