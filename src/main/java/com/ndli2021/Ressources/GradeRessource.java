package com.ndli2021.Ressources;
import com.ndli2021.models.Grade;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/grades")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GradeRessource {

    @GET
    public List<Grade> list(){
        return Grade.listAll();
    }

    @GET
    @Path("/{id}")
    public Grade get(@PathParam("id") Long id) {
        return Grade.findById(id);
    }

    @POST
    @Transactional
    public Response create(Grade grade) {
        grade.persist();
        return Response.created(URI.create("/grades/" + grade.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public Grade update(@PathParam("id") Long id, Grade grade) {
        Grade entity = Grade.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.nom = grade.nom;
        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        Grade entity = Grade.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/count")
    public Long count() {
        return Grade.count();
    }
}
