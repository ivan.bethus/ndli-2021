package com.ndli2021.Ressources;
import com.ndli2021.models.Bateaux;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/bateaux")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BateauxRessources {

    @GET
    public List<Bateaux> list(){
        return Bateaux.listAll();
    }

    @GET
    @Path("/{id}")
    public Bateaux get(@PathParam("id") Long id) {
        return Bateaux.findById(id);
    }

    @POST
    @Transactional
    public Response create(Bateaux bateaux) {
        bateaux.persist();
        return Response.created(URI.create("/bateaux/" + bateaux.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public Bateaux update(@PathParam("id") Long id, Bateaux bateaux) {
        Bateaux entity = Bateaux.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.nom = bateaux.nom;
        entity.description = bateaux.description;

        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        Bateaux entity = Bateaux.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/search/{name}")
    public Bateaux search(@PathParam("name") String name) {
        return Bateaux.findByName(name);
    }

    @GET
    @Path("/count")
    public Long count() {
        return Bateaux.count();
    }
}
