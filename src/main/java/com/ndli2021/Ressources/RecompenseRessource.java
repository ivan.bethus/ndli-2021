package com.ndli2021.Ressources;
import com.ndli2021.models.Recompenses;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/recompenses")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RecompenseRessource {

    @GET
    public List<Recompenses> list(){
        return Recompenses.listAll();
    }

    @GET
    @Path("/{id}")
    public Recompenses get(@PathParam("id") Long id) {
        return Recompenses.findById(id);
    }

    @POST
    @Transactional
    public Response create(Recompenses recompense) {
        recompense.persist();
        return Response.created(URI.create("/recompenses/" + recompense.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public Recompenses update(@PathParam("id") Long id, Recompenses recompense) {
        Recompenses entity = Recompenses.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.nom = recompense.nom;
        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        Recompenses entity = Recompenses.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/count")
    public Long count() {
        return Recompenses.count();
    }
}
