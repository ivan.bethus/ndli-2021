package com.ndli2021.Ressources;
import com.ndli2021.models.Temoignage;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/temoignages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TemoignageRessource {

    @GET
    public List<Temoignage> list(){
        return Temoignage.listAll();
    }

    @GET
    @Path("/{id}")
    public Temoignage get(@PathParam("id") Long id) {
        return Temoignage.findById(id);
    }

    @POST
    @Transactional
    public Response create(Temoignage temoignage) {
        temoignage.persist();
        return Response.created(URI.create("/temoignages/" + temoignage.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public Temoignage update(@PathParam("id") Long id, Temoignage temoignage) {
        Temoignage entity = Temoignage.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.auteur = temoignage.auteur;
        entity.description = temoignage.description;

        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        Temoignage entity = Temoignage.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/count")
    public Long count() {
        return Temoignage.count();
    }
}
