package com.ndli2021.Ressources;
import com.ndli2021.models.FamilleSauveteur;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/famille_sauvetages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FamilleSauveteurRessource {

    @GET
    public List<FamilleSauveteur> list(){
        return FamilleSauveteur.listAll();
    }

    @GET
    @Path("/{id}")
    public FamilleSauveteur get(@PathParam("id") Long id) {
        return FamilleSauveteur.findById(id);
    }

    @POST
    @Transactional
    public Response create(FamilleSauveteur familleSauvetage) {
        familleSauvetage.persist();
        return Response.created(URI.create("/famille_sauvetage/" + familleSauvetage.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public FamilleSauveteur update(@PathParam("id") Long id, FamilleSauveteur familleSauvetage) {
        FamilleSauveteur entity = FamilleSauveteur.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.nom = familleSauvetage.nom;
        entity.description = familleSauvetage.description;

        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        FamilleSauveteur entity = FamilleSauveteur.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/search/{name}")
    public FamilleSauveteur search(@PathParam("name") String name) {
        return FamilleSauveteur.findByName(name);
    }

    @GET
    @Path("/count")
    public Long count() {
        return FamilleSauveteur.count();
    }
}
