package com.ndli2021.Ressources;
import com.ndli2021.models.FamilleBateaux;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/famille_bateaux")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FamilleBateauxRessource {

    @GET
    public List<FamilleBateaux> list(){
        return FamilleBateaux.listAll();
    }

    @GET
    @Path("/{id}")
    public FamilleBateaux get(@PathParam("id") Long id) {
        return FamilleBateaux.findById(id);
    }

    @POST
    @Transactional
    public Response create(FamilleBateaux familleSauvetage) {
        familleSauvetage.persist();
        return Response.created(URI.create("/famille_bateaux/" + familleSauvetage.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public FamilleBateaux update(@PathParam("id") Long id, FamilleBateaux familleSauvetage) {
        FamilleBateaux entity = FamilleBateaux.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.nom = familleSauvetage.nom;
        entity.description = familleSauvetage.description;

        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        FamilleBateaux entity = FamilleBateaux.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/search/{name}")
    public FamilleBateaux search(@PathParam("name") String name) {
        return FamilleBateaux.findByName(name);
    }

    @GET
    @Path("/count")
    public Long count() {
        return FamilleBateaux.count();
    }
}
