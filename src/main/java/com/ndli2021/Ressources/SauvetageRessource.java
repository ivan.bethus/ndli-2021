package com.ndli2021.Ressources;
import com.ndli2021.models.Sauvetage;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/api/sauvetages")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SauvetageRessource {

    @GET
    public List<Sauvetage> list(){
        return Sauvetage.listAll();
    }

    @GET
    @Path("/{id}")
    public Sauvetage get(@PathParam("id") Long id) {
        return Sauvetage.findById(id);
    }

    @POST
    @Transactional
    public Response create(Sauvetage sauvetage) {
        sauvetage.persist();
        return Response.created(URI.create("/sauvetages/" + sauvetage.id)).build();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public Sauvetage update(@PathParam("id") Long id, Sauvetage sauvetage) {
        Sauvetage entity = Sauvetage.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        // map all fields from the person parameter to the existing entity
        entity.nom = sauvetage.nom;
        entity.description = sauvetage.description;

        return entity;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        Sauvetage entity = Sauvetage.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }

    @GET
    @Path("/search/{name}")
    public Sauvetage search(@PathParam("name") String name) {
        return Sauvetage.findByName(name);
    }

    @GET
    @Path("/count")
    public Long count() {
        return Sauvetage.count();
    }
}
