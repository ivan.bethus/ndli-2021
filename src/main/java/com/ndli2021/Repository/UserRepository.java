package com.ndli2021.Repository;

import com.ndli2021.models.CustomUser;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<CustomUser> {

    public UserRepository() {
        // TODO Auto-generated constructor stub
    }

    public CustomUser findByUsername(String username){
        return find("username", username).firstResult();
    }

}
