package com.ndli2021.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Temoignage extends PanacheEntity {
    public String auteur;
    public String description;
}
