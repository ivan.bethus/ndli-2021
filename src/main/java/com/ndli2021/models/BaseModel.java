package com.ndli2021.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public class BaseModel extends PanacheEntity {
    @NotNull
    @Enumerated(EnumType.STRING)
    public Statut statut;
}
