package com.ndli2021.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity
public class Sauveteur extends BaseModel {
    public String prenom;
    public String description;
    public Date dateNaissance;
    public Date dateMort;
    @ManyToOne
    public FamilleSauveteur famille;
    @ManyToOne
    public Bateaux bateaux;
    @OneToMany
    public List<Sauvetage> sauvetages;
    @OneToMany
    public List<Recompenses> recompenses;
    @OneToMany
    public List<Temoignage> temoignages;

    public static Sauveteur findByName(String nom){
        return find("nom", nom).firstResult();
    }
}
