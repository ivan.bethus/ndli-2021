package com.ndli2021.models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Bateaux extends BaseModel {
    public String nom;
    public Date dateCreation;
    public String description;
    @ManyToOne
    public FamilleBateaux famille;

    public static Bateaux findByName(String nom){
        return find("nom", nom).firstResult();
    }
}
