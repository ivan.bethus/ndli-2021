package com.ndli2021.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Sauvetage extends BaseModel {
    public String nom;
    public String description;
    public Date date;
    public String personnesSauvees;
    public String duree;
    @ManyToOne
    public Bateaux bateaux;

    public static Sauvetage findByName(String nom){
        return find("nom", nom).firstResult();
    }
}
