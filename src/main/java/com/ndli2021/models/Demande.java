package com.ndli2021.models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Demande extends BaseModel {
    @ManyToOne
    public CustomUser user;
    @OneToOne
    public Sauveteur sauveteur;
    @OneToOne
    public Sauvetage sauvetage;
    @OneToOne
    public Bateaux bateaux;

    public static Demande findByName(String nom){
        return find("nom", nom).firstResult();
    }
}
