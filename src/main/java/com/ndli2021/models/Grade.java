package com.ndli2021.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Grade extends PanacheEntity {
    public String nom;

    public static Grade findByName(String nom){
        return find("nom", nom).firstResult();
    }
}
