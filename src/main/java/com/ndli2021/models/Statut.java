package com.ndli2021.models;

public enum Statut {
    EN_COURS,
    EN_LIGNE,
    ANNULE
}
