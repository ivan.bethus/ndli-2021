package com.ndli2021.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class FamilleSauveteur extends PanacheEntity {
    public String nom;
    public String description;

    public static FamilleSauveteur findByName(String nom){
        return find("nom", nom).firstResult();
    }
}
