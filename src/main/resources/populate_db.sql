INSERT INTO famillebateaux (id, description, nom)
VALUES (0, 'La plus grande famille de bateaux', 'Les drakars');

INSERT INTO bateaux (id, nom, statut, datecreation, description, famille_id)
VALUES (0, 'le vaillant', 'EN_COURS', now(), 'un bien beau bateau', 0);

INSERT INTO famillesauveteur (id, description, nom)
VALUES (0, 'Une belle et grande famille', 'BETHUS');

INSERT INTO grade (id, nom)
VALUES (0, 'capitaine');

INSERT INTO recompenses (id, nom)
VALUES (0, 'Protégé de la reine');

INSERT INTO sauvetage (id, date, description, duree, nom, personnessauvees, bateaux_id)
VALUES (0, now(), 'une sauvetage bien périlleux', '3h heures', 'Sauvetage de l amarillis', 'Equipage sauvé', 0);

INSERT INTO sauveteur (id, datemort, datenaissance, description, prenom, bateaux_id, famille_id)
VALUES (0, now(), now(), 'Un homme bien brave', 'Ivan', 0, 0);

INSERT INTO temoignage (id, auteur, description)
VALUES (0, 'Baron de Paris', 'Ivan est un homme incroyable, je n en reviens pas !');

INSERT INTO sauveteur_recompenses (sauveteur_id, recompenses_id)
VALUES (0, 0);

INSERT INTO sauveteur_sauvetage (sauveteur_id, sauvetages_id)
VALUES (0, 0);

INSERT INTO sauveteur_temoignage (sauveteur_id, temoignages_id)
VALUES (0, 0);